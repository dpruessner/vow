# Vow - Promises in Ruby 
... using EventMachine or a provided ThreadPool for deferment.  They're still called *Promise*,
despite the name of this library.

Promises execute in a non-blocking way-- only blocking on synchronization structures
during creation and chaining.  All code blocks execute in a deferred context, either through
`EM.next_tick {}` or through a `ThreadPool.do_next {}`.

## ThreadPool
Threads are actually pretty light.  Not as light as the Fibers used in EM, but not particularly
heavy in Linux.  Many lightweight computers (Single Board Computers) will have a few hundred threads
running.  Many desktop applications will have several thousand threads.

Thread pool has an upper limit (`ThreadPool.thread_limit`), and compares this against the number
of available workers when a `::do_next {}` block is called.  If there are no immediately available
worker threads and the limit is not reached, then a new thread is created to process the request.

If the pool has the maximum number of threads created, then the block will be pushed to the
back of the synchronization queue.  This means that the block will not be executed until other
(probably blocking) blocks are executed.  It is better to set the
`ThreadPool.thread_limit` higher than lower.  

Since idle worker theads are waiting on a Ruby synchronization `Queue`, the threads are in a 
suspended state.  This means that they are not taxing the Kernel scheduler for building the thread
run queue-- they only consume the thread stack memory and thread structures in the kernel.

## Timeouts and Scheduled
Timeouts and scheduled actions currently occupy an individual thread each.  So adding lots
of timeouts will begin to block `::do_next {}` blocks.

**TODO**: This should be reimplemented later as a single system: Timeouts are a special
case of a Scheduled-- where the Timeout calls a `reject` on the relevant Promise.  Scheduled
items should be held in a priority Queue and updates to the Queue would notify a single
thread listener that is responsible for dispatching the (quick!) failures or queue
the action for next execution (via `::do_next`).

## License
This project is copyright Daniel Pruessner, 2017.  It is not yet released under
any license that allows copying or distribution.
