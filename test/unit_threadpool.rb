#!/usr/bin/env ruby

require 'minitest/autorun'
begin
  basedir = File.expand_path( '../..', __FILE__ )
  $:.unshift basedir + '/lib'
end
require 'vow'

Vow::ThreadPool::LOG.level = :none
Vow::LOG.level = :none

Thread.abort_on_exception = true

def backtrace
  begin
    raise RuntimeError.new
  rescue => e
    bt = e.backtrace
    bt.shift
    return bt
  end
end


class ThreadPoolTest < Minitest::Test

  # This sucks.  It has to be one big-ass function because order does matter.

  def test_run
    # Verify first-run has no threads
    begin
      assert_equal 0, Vow::ThreadPool.thread_count
    end

    # Verify it runs a block and runs with one thread
    begin
      is_success = false

      Vow::ThreadPool.next_do { is_success = true }
      Thread.pass
      sleep 0.1
      assert is_success
      assert_equal 2, Vow::ThreadPool.thread_count
    end

    # Verify it runs a block and adds multiple threads
    begin
      my_count = 0
      2.times{ Vow::ThreadPool.next_do { sleep 0.10; my_count += 1 } }
      Thread.pass
      sleep 0.2
      assert_equal 3, Vow::ThreadPool.thread_count
      assert_equal 2, my_count
    end

    # Verify that a thread limit is hit; that ones past the limit take longer to run
    begin
      my_count = 0
      Vow::ThreadPool.thread_limit = 10 
      ts = Time.now
      lock = Mutex.new
      hsh = {}

      12.times do |idx| 
        Vow::ThreadPool.next_do do
          sleep 0.1
          lock.synchronize do
            dt = Time.now.-(ts).*(1e3).round(2)
            hsh[idx] = dt
            #_puts("Block #{idx} took #{dt}ms")
          end
        end
      end
      Thread.pass
      dt = Time.now.-(ts).*(1e3).round(2)
      assert dt  < 5    # Expect that creating deferreds is fast
      #_puts "DT is actually: #{dt}ms"
      sleep 1

      assert_equal 10, Vow::ThreadPool.thread_count
      assert_equal 12, hsh.length
      
      assert hsh[11] > (1.5*hsh[0])    # Make sure the last item waited in the queue
      #_puts "hsh: #{hsh.inspect}"
    end

  end

end
