#!/usr/bin/env ruby
#
require 'thread'
require 'timeout'
require 'minitest/autorun'
require 'minitest/spec'
require 'pry'

begin
  basedir = File.expand_path( '../..', __FILE__ )
  $:.unshift basedir + '/lib'
  end
require 'vow'

Thread.abort_on_exception = true
vow_lock                  = Mutex.new

Vow::ThreadPool::LOG.level = :none
Vow::LOG.level = :none

#
# NOTE: there are a nubmer of synchronization mechanisms put in place (extra) to make sure
# the blocks execute in the right order for testing the framework.
#


describe Vow::Promise, "Main promise" do

  describe "#new" do
    before { vow_lock.lock; @promise = Vow::Promise.new{ |f,r| @f = f; @r = r ; nil} }
    after { vow_lock.unlock }

    it "has state pending on new" do
      @promise.state.must_equal :pending
    end
  end

  describe "basic promise API" do
    it "creates a fulfilled promise with Promise.fulfill" do
      vow_lock.synchronize do
        p = Vow::Promise.fulfill(42)
        p.state.must_equal :fulfilled
        p.value.must_equal 42
        p.settle(1)
        p.value.must_equal 42         # Only settled once.
      end
    end
    it "creates a rejected promise with Promise.reject" do
      vow_lock.synchronize do
        p = Vow::Promise.reject RuntimeError
        p.state.must_equal :rejected
        p.reason.must_equal RuntimeError
      end
    end

  end

  describe "fulfilled promise" do
    before do 
      vow_lock.lock
      @event = Vow::Event.new
      @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; @event.set; nil }
      @event.wait
      @f[true]
    end
    after { vow_lock.unlock }

    it "has fulfilled state" do
      @promise.value                           # force resolution
      @promise.state.must_equal :fulfilled
    end
    it "has a value" do
      @promise.value                           # force resolution
      @promise.value.must_equal true
    end
    it "cannot be re-set" do
      @promise.value                           # force resolution
      @f[false]
      @promise.value.must_equal true
      @r[false]
      @promise.state.must_equal :fulfilled
    end
  end

  describe "rejected promise" do

    describe "with reject function" do
      before do
        vow_lock.lock
        event = Vow::Event.new
        @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; event.set; nil }
        event.wait
        @r[:reason]
      end
      after { vow_lock.unlock }

      it "has a rejected state" do
        @promise.wait                         # force resolution
        @promise.state.must_equal :rejected
      end
      it "has a reason" do
        @promise.wait                         # force resolution
        @promise.reason.must_equal :reason
      end
    end

    describe "with exception in promise block" do
      before do
        vow_lock.lock
        @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; raise RuntimeError.new }
      end
      after { 
        vow_lock.unlock 
      }

      it "has a rejected state" do
        assert_raises RuntimeError do
          @promise.value                         # force resolution
        end
        @promise.state.must_equal :rejected
      end
      it "has a reason equal to the raised exception" do
        assert_raises RuntimeError do
          @promise.value                         # force resolution
        end
        @promise.reason.must_be_kind_of RuntimeError
      end
      it "cannot be re-set" do
        assert_raises RuntimeError do
          @promise.value                         # force resolution
        end
        @f[true]
        @promise.state.must_equal :rejected

        assert_raises RuntimeError do
          @promise.value
        end
      end
    end
  end

  describe "chained promises" do
    describe "with Then block" do
      describe "with unfulfilled promise" do
        before do
          vow_lock.lock
          @success = false
          @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; nil }
          @then = @promise.then { @success = true }
          20.times{ Thread.pass }                                   # Nothing is running, so no events; allow time to pass
        end
        after { vow_lock.unlock }
        it "does not execute Then block" do
          @success.must_equal false
        end
        it "must have Then pending state" do
          @then.state.must_equal :pending
        end
      end

      describe "with fulfilled promise" do
        before do
          vow_lock.lock
          @success = false
          @event = Vow::Event.new
          @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; e = @event; @event = Vow::Event.new; e.set } # Perform event swap
          @then = @promise.then { @success = true; @event.set }
          @event.wait(1)
          @f[true]
          @event.wait(1)
        end
        after { vow_lock.unlock }

        it "has executed Then block" do
          @success.must_equal true
        end
        it "must have Then fulfilled state" do
          @then.state.must_equal :fulfilled
        end
      end

      describe "with rejected promise" do
        before do
          vow_lock.lock
          @success = false
          @event   = Vow::Event.new
          @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; @event.set }
          @then = @promise.then { @success = true }
          @event.wait(1)
          @r[false]
          20.times{ Thread.pass }
        end
        after{ vow_lock.unlock }

        it "does not execute Then block" do
          @success.must_equal false
        end
        it "has rejected Then block" do
          @then.state.must_equal :rejected
        end
        it "gives Then block the reason" do
          @then.reason.must_equal false
        end
      end
    end

    describe "with Then-Error block" do
      describe "with fulfilled Promise" do
        before do
          vow_lock.lock
          @is_caught = false
          @event   = Vow::Event.new
          @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; @event.set }
          @then = @promise.then(Proc.new{}) { @is_caught = true }
          @event.wait(5)
          @f[100]
          20.times{ Thread.pass }
        end
        after{ 
          vow_lock.unlock 
        }

        it "does not execute catch block" do
          @is_caught.must_equal false
        end
        it "has state fulfilled" do
          @then.state.must_equal :fulfilled
        end
      end

      describe "with rejected Promise" do
        before do
          vow_lock.lock
          @is_caught = false
          @event   = Vow::Event.new
          @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; @event.set }
          @then = @promise.then(Proc.new{}) { @is_caught = true }
          @event.wait(1)
          @r[-1]
          20.times{ Thread.pass }

        end
        after{ vow_lock.unlock }

        it "executes Then-Error block" do
          @is_caught.must_equal true
        end
        it "sets Then-Error state to fulfilled" do
          @then.state.must_equal :fulfilled
        end
        it "sets Then-Error value to block return" do
          @then.value.must_equal true
        end
      end

      describe "with 'catch' method" do
        describe "with fulfilled Promise" do
          before do
            vow_lock.lock
            @is_caught = false
            @event   = Vow::Event.new
            @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; @event.set }
            @then = @promise.catch { @is_caught = true }
            @event.wait(5)
            @f[100]
            20.times{ Thread.pass }
          end
          after{ 
            vow_lock.unlock 
          }

          it "does not execute catch block" do
            @is_caught.must_equal false
          end
          it "has state fulfilled" do
            @then.state.must_equal :fulfilled
          end
        end

        describe "with rejected Promise" do
          before do
            vow_lock.lock
            @is_caught = false
            @event   = Vow::Event.new
            @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; @event.set }
            @then = @promise.catch { @is_caught = true }
            @event.wait(1)
            @r[-1]
            20.times{ Thread.pass }

          end
          after{ vow_lock.unlock }

          it "executes Then-Error block" do
            @is_caught.must_equal true
          end
          it "sets Then-Error state to fulfilled" do
            @then.state.must_equal :fulfilled
          end
          it "sets Then-Error value to block return" do
            @then.value.must_equal true
          end
        end
      end

      ####
    end

    describe "with lots of chains" do
      before do
        vow_lock.lock
        @stage = []
        @event = Vow::Event.new
        Vow::ThreadPool.reset!
        @promise = Vow::Promise.new{|f,r| @f = f; @event.set}
        s = @promise

        # Add in [0, ..., 9] to @stage
        10.times{|i| s = s.then{ @stage << i } }
        #@promise.then { @stage << 1 }.then{ @stage << 2 }.then{ @stage << 3 }.then{ @stage << 4 }.then{ @stage << 5 }.then{ @stage << 6 }.then{ @stage << 7 }
        @event.wait(1)
        @f[1]
        20.times{ Thread.pass }
      end
      after { vow_lock.unlock }
      it "executes all stages" do
        @stage.must_equal [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
      end
      it "must not use up more than 2 threads" do
        #puts "***** Thread count/waiting: #{Vow::ThreadPool.thread_count}/#{Vow::ThreadPool.waiting_count} *****"
        assert Vow::ThreadPool.thread_count <= 3
      end
    end




    describe "with Exceptional Then-Error" do
      before do
        vow_lock.lock
        @is_caught = false
        @event   = Vow::Event.new
        @promise = Vow::Promise.new {|f,r|  @f = f; @r = r; @event.set }
        @then = @promise.then(Proc.new{raise RuntimeError.new }) { @is_caught = true }
        @event.wait(1)
        @f[1]
        20.times{ Thread.pass }
      end
      after{ vow_lock.unlock }

      it "does not execute catch block" do
        @is_caught.must_equal false
      end
      it "has Then-Else with rejected state" do
        @then.state.must_equal :rejected
      end
      it "has Then-Else with rejected reason" do
        @then.reason.must_be_kind_of RuntimeError
      end
    end
  end

  describe "with complex fulfillments" do
    before(:all) do
      vow_lock.lock
      @is_success = false
      @promise = Vow::Promise.new{|f,r| Thread.new{ sleep 0.2; f[true]} }
      @then = @promise.then { @is_success = true }

      @ts = Time.now
      @final_value = @then.value(0.3)  # Wait in this thread for the value...
      @dt = Time.now - @ts
    end
    after { vow_lock.unlock }

    it "executes the chain" do
      @is_success.must_equal true
    end
    it "must have waited synchronously" do
      @dt.must_be :>, 0.2
      @dt.must_be :<, 0.45
    end
    it "must set final_value" do
      @final_value.must_equal true
    end
  end
  describe "with retry ability" do
    before do
      vow_lock.lock
      @closure_count = 0
      @failing_action = Proc.new do # Fails first 2 calls
        sleep 0.1
        @closure_count += 1
        raise RuntimeError.new("closure count too low: #{@closure_count}") unless @closure_count > 2
        100
      end
    end
    after { vow_lock.unlock }

    it "retries too few times and fails" do
      @closure_count = 0
      p = Vow::Promise.retry(retries: 1){ |f,r| f[ @failing_action.call ] }
      assert_raises RuntimeError do
        p.value
      end
      p.reason.wont_be_nil
    end
    it "retries the right number of times" do
      @closure_count = 0
      p = Vow::Promise.retry(retries: 3){ |f,r| f[ @failing_action.call ] }
      p.value.must_equal 100
      p.reason.must_be_nil
    end
  end
  describe "retry with timeout" do
    before do
      vow_lock.lock
      @closure_count = 0
      @failing_action = Proc.new do # Fails first 2 calls
        @closure_count += 1
        sleep_time = 0.5 - (0.1 * @closure_count)
        #_puts "failing_action / #{@losure_count} (sleep: #{sleep_time})"
        sleep sleep_time
        100
      end
    end
    after { vow_lock.unlock }

    it "retries too few times and fails" do
      #_puts "Trying with timeout and fails"
      @closure_count = 0
      p = Vow::Promise.retry(retries: 1){|f,r|  f[ Vow::Promise.new{|f,r| f[ @failing_action.call ] }.timeout!(0.2) ]  }
      assert_raises RuntimeError do
        p.value(4).must_be_nil
      end
      p.reason.wont_be_nil
      #_puts ""
      $_puts_enabled = false
    end
    it "retries the right number of times" do
      Vow::ThreadPool.reset!
      #_puts "Trying with timeout and success"
      @closure_count = 0
      p = Vow::Promise.retry(retries: 3){|f,r| f[ Vow::Promise.new{|f,r| f[ @failing_action.call ] }.timeout!(0.2) ]  }
      p.value(4).must_equal 100
      p.reason.must_be_nil
      $_puts_enabled = false
      #puts "Threads used: #{Vow::ThreadPool.thread_count} / #{Vow::ThreadPool.waiting_count} waiting"
    end
  end

  describe "Promise.all" do
    before{ vow_lock.lock; Vow::LOG.level = :none; Vow::ThreadPool::LOG.level = :none; Vow::ThreadPool.reset!; }
    after{ vow_lock.unlock; Vow::LOG.level = :none; Vow::ThreadPool::LOG.level = :none;}

    it "fulfills when all children fulfill" do
      promises = []
      promises << Vow::Promise.new{|f| f[true] }
      promises << Vow::Promise.new{|f| sleep 0.1; f[true] }
      promises << Vow::Promise.new{|f| f[ Vow::Promise.fulfill(90) ] }

      p = Vow::Promise.all( *promises )
      p.wait

      p.fulfilled?.must_equal true
      p.value.must_equal [true, true, 90 ]
    end

    it "rejects when any child rejects" do
      promises = []
      promises << Vow::Promise.new{|f| f[true] }
      promises << Vow::Promise.new{|f| sleep 0.1; f[true] }
      promises << Vow::Promise.new{|f| raise RuntimeError.new }

      p = Vow::Promise.all( *promises )
      p.wait
      p.rejected?.must_equal true
      assert_raises RuntimeError do
        p.value 
      end
      p.state.must_equal :rejected
    end
  end

  describe "Promise.race" do
    before{ vow_lock.lock; Vow::LOG.level = :none; Vow::ThreadPool::LOG.level = :none; Vow::ThreadPool.reset!; }
    after{ vow_lock.unlock; Vow::LOG.level = :none; Vow::ThreadPool::LOG.level = :none;}

    it "fulfills when first child fulfills" do
      promises = []
      promises << Vow::Promise.new{|f| sleep 0.1; raise RuntimeError.new }
      promises << Vow::Promise.new{|f| sleep 0.1; f[true] }
      promises << Vow::Promise.new{|f| sleep 0.05; f[42]  }

      p = Vow::Promise.race( *promises )
      p.wait

      20.times{ Thread.pass }

      p.fulfilled?.must_equal true
      p.value.must_equal 42
    end
    it "rejects when first child rejects" do
      promises = []

      promises << Vow::Promise.new{|f| sleep 0.05; raise RuntimeError.new }
      promises << Vow::Promise.new{|f| sleep 0.1; f[true] }
      promises << Vow::Promise.new{|f| sleep 0.1; f[42]  }

      p = Vow::Promise.race( *promises )
      p.wait

      20.times{ Thread.pass }

      p.rejected?.must_equal true
      p.reason.must_be_kind_of RuntimeError
    end
  end


end


__END__








  end
end

describe Vow::Promise, "with threads" do
  describe "deferred fulfilment" do
    before(:all) do
      Timeout::timeout(0.1) do
        @success = false
        @promise = Vow::Promise.new{|f,r| @f = f; @r = r; nil }
        @ts = Time.now
        @thread = Thread.new{ @f[true] }
        @thread.join
        @te = Time.now - @ts
      end
    end
    it "#state is :fulfilled" do
      @promise.state.must_equal :fulfilled
    end
    it "should execute fast" do
      @te.must_be :<, 0.05
    end
  end


  describe "with retry ability" do
    before do
      timeout = 0.4

      closure_var = 3
      closure_proc = Proc.new{ closure_var -= 1;  raise RuntimeError.new("var is #{closure_var}") if closure_var > 0; 42; }

      @promise = Vow::Promise.new{|f,r| @f = f; @r = r; nil }
      retries = 5

      Thread.new do
        loop do
          break if (retries == 0)
          retries -= 1
          p = Vow::Promise.new(new_thread:true){|f,r| 
            _puts("x!")
              Timeout::timeout(timeout){ f[ closure_proc[] ] } 
          }
          _ts = Time.now
          p.value  # Wait on timeout or return
          _dt = Time.now - _ts
          _puts "#{retries}: Thread recounter value-wait: #{_dt.*(1e3)}ms"
          if p.state == :rejected
            _puts "reason: #{p.reason}"
          else
            _puts "Got value: #{p.value}"
            @f[ p.value ]
            break
          end
        end
        @r[ 'too many retries' ]
      end
    end
    it "works" do
    end
  end

end
