
module Vow
  LOG = MyLogger.new 'Vow'
  #LOG = Logging.logger['Vow ']
  LOG.level = :error

  ## Dispatch an event to a mechanism for deferred (but very, very soon) execution
  #
  # Thread.current[:defer_strategy] determines what service to use. 
  #
  # when `Thread.current[:defer_strategy] == :em`, then `EM.next_tick(...)` is called.
  # Otherwise, `ThreadPool.next_do(...)` is called.
  #
  #
  module Deferable

    ## Preform dispatch via EM or ThreadPool
    #
    # @return [nil]
    #
    def defer &blk
      case Thread.current[:defer_strategy]
      when :em
        EM.next_tick( &blk )
      when :threadpool, nil
        LOG.warn{"*** Deferable: calling #defer for blk at: #{blk.source_location}"}
        ThreadPool.next_do( &blk )
      end
      nil
    end
  end

  ## One-shot event class
  # 
  # Uses a Mutex (synchronizable) and a ConditionVariable (for anyone waiting on event).
  #
  # `Event` is used to update *fulfilled* or *rejected* state of a `Promise`.
  #
  class Event
    def initialize
      @lock = Mutex.new
      @cond = ConditionVariable.new
      @flag = false
    end
    
    ## Perform one-shot set action
    #
    def set
      @lock.synchronize do
        @flag = true
        @cond.broadcast
     end
    end

    ## Wait on the one-shot action
    #
    def wait(timeout=nil)
      begin
      @lock.synchronize do
        LOG.debug{ "Event#wait: @flag=#{@flag}" }
        if not @flag
          LOG.debug{ "Event#wait: THREADS are: #{Thread.list.inspect}" }
          LOG.debug{ "Event#wait: waiting on CVAR" }
          @cond.wait(@lock, timeout)
          LOG.debug{ "Event#wait: woke from      CVAR" }
        end
      end
      rescue Exception => e
        LOG.error{"Thread#{Thread.current}: Error on wait: #{e.class}; #{e}"}
        LOG.error{"Backtrace: #{e.backtrace.join("\n\t")}"}
        sleep 0.1
        raise e
      end
    end
  end   # class Event


  ## Capabilities of an object to synchronize to a Mutex
  #
  module Synchronizable
    def synchronize
      if @mutex
        @mutex.synchronize do
          yield
        end
      else
        yield
      end
    end
  end

  class UncaughtPromiseError < StandardError; end
  

  ## Promises in Ruby.
  #
  # This is an implementation for Promises in Ruby that behave like you would expect when coming from
  # a ECMAScript/Harmony or A+ Promises world.  
  #
  # Other promise implementations do crazy things like blow up when `#settle` (or its analog) is called
  # on an already-resolved `Promise`.  That shouldn't happen.  All promise settlement should be one-shot
  # and should *NOP* on repeated settlement attempts.  By not blowing up, we can create timeouts,
  # races, and other constructs.
  #
  # ## Notes about `#value`
  #
  # Promises should be treated carefully with regards to `#value`.  This pulls out the *fulfilled* `Promise`s
  # value.  A *rejected* promise does not have a value-- it has an exception.  The exception is either:
  #
  # 1. The exception that was raised by the block executed by the `Promise`; or--
  # 2. a `UncaughtPromiseError` exception that wraps the rejection reason.  This causes errors
  #    fail quickly and loudly to minimize latent, unexpected errors.
  #
  # sooo... to be safe, always provide a `#catch{...}` to your Promise... or use `#tap_fulfilled{|value| ...}`,
  # which may not be executed... or `#value_with_default(obj)`.
  #
  #
  class Promise
    @@mutex = Mutex.new
    @@id    = 0

    ## Thread-safe create next ID for Promises.  Helps for debugging.
    #
    def self.get_id
      @@mutex.synchronize do
        @@id += 1
        return @@id
      end
    end


    include Synchronizable
    include Deferable

    PASSTHROUGH_EXCEPTIONS = [NoMemoryError, SignalException, SystemExit] 


    ## Create a retrying promise.  
    #
    # Block is yielded `fulfill` and `reject` funcion references like `#new`.
    #
    def self.retry(retries:0, &blk)
      p = nil
      LOG.info{"Promise.retry(...): retries=#{retries}. Entering."}
      p = Vow::Promise.new do |f, r|
        loop do
          LOG.debug{"retry: loop entering..."}
          lp = Vow::Promise.new( &blk )
          LOG.debug{"retry: Created new promise #{lp.inspect}"}
          lp.wait   # Wait on the value

          if lp.state == :fulfilled
            f[ lp.value ]
            break
          else
            ( r[ lp.reason ]; break) if retries == 0
            retries -= 1
          end

        end # loop
      end   # Vow::Promise.new
      LOG.info{"Promise.retry(...); returning p=#{p.inspect}"}
      p
    end

    ## Create a promise that has already been fulfilled with the given value.
    #
    # Sometimes it makes sense to return a Promise with a value.  This is a 
    # wrapper that will allow you to take any value and treat it as a promise.
    #
    # @note
    #
    # @param [Object] val any value
    # 
    # @return [Promise] that is `#state == :fulfilled` and `#value == val` (if val is not a Promise).  Otherwise, this is a NO-OP and the promise is returned.
    #
    def self.fulfill(val)
      return val if val.kind_of? Promise
      self.new(value:val)
    end

    ## Create a promise that is already rejected.  This will cascade rejections and catches.
    #
    # @param [Object] reason any reason for rejection
    #
    # @return [Promise] that is `#state == :rejected`
    #
    def self.reject(reason = false)
      reason = false if reason.nil?
      self.new(reason:reason)
    end


    ## Creates a new promise that is fulfilled only if all the listed promises are fulfilled.
    #
    # The promise is rejected with the first rejection in the list.
    #
    # @overload all( promise_ary )
    #   @param promise_ary [Array<Promise>]
    #
    #
    # @overload all( *promises )
    #   @param promises [Promise]
    #
    # @return [Promise]
    # 
    def self.all(*args)

      ary = (args[0].kind_of?(Array) ? args[0] : args)

      promise = Promise.new
      oneshot = false

      settle_block = Proc.new do |p, val, reason|
        next if oneshot
        if reason # settle is a failure
          promise.reject(reason)
        else
          if ary.all?{|e| e.state == :fulfilled}
            promise.fulfill( ary.map{|e| e.value } )
          end
        end
      end

      ary.each do |p|
        p.add_observer{ |f,r| settle_block[p,f,r] }  # Each settlement will notify the settle block above.
      end

      return promise
    end

    ## Creates a new promise that is fulfilled/rejected when the first provided promise is fulfilled/rejected.
    #
    # @overload race( promise_ary )
    #   @param [Array<Promise>] promise_ary
    #
    # @overload race( *promises )
    #
    # @return [Promise]
    #
    def self.race(*args)
      ary = (args[0].kind_of?(Array) ? args[0] : args )

      promise = Promise.new
      ary.each{|p| p.add_observer( promise.method(:settle) ) }
      
      promise
    end




    ## Create a new Promise
    #
    # 
    def initialize(parent:nil, errback:nil, callback:nil, value:nil, reason:nil, &blk)
      LOG.debug{"Promise.new: parent=#{parent.inspect}"}

      @_id = self.class.get_id

      @blk   = blk
      @value = nil                ## set when Promise is fulfilled
      @state = :pending           ## = (:pending, :fulfilled, :rejected)
      @reason = nil               ## set when Promise is rejected
      @mutex = Mutex.new
      @observers = []             # Array<settleable>

      @reject = self.method(:reject)
      @fulfill = self.method(:fulfill)

      @parent = parent

      @callback = callback
      @errorback = errback

      @event = Event.new

      unless value.nil?
        @value = value
        @state = :fulfilled
        return self
      end

      unless reason.nil?
        @reason = reason
        @state = :rejected
        return self
      end

      if @blk 
        defer do
          begin
            @blk.call @fulfill, @reject
          rescue *PASSTHROUGH_EXCEPTIONS  => e
            LOG.error{"!!EXCEPTION!!: Rescue/RAISE passthrough: #{e.class}: #{e.to_s.inspect}"}
            raise e
          rescue => e
            LOG.info{"Promise=#{self.inspect} @blk exception: #{e.class}:#{e.to_s.inspect} :::#{e.backtrace.join("\n\t")}..."}
            settle(nil, e)
          end
        end
      end
    end

    ## Returns a debug-friendly string describing the Promise.
    #
    # @return [String]
    #
    def inspect
      "#<#{self.class}:#{"0x%02x" % @_id} s:#{@state} v:#{@value.inspect} r:#{@reason.inspect}>" 
    end

    ## Set a timeout on a promise.
    # 
    # This may be a really bad idea.  All the other blocks in the chain (preceeding this timeout!)
    # will continue to execute.  For example if:
    #
    #     p = new_user_promiser.                                                # p1
    #           then{|user| user.destroy_stuff; user}.                          # p2
    #           then{|user| user.long_method}.timeout!(5).catch{|e|  ..... }    # p3, c1
    #
    # if the first `new_user_promiser` promise is the one that takes forever (and the timeout fires),
    # then **p2** and **p3** will still execute.  In fact, **p2** and **p3** will likely execute while
    # catch **c1** executes at the same time.  The `timeout!(...).catch{...}` blocks just pre-empt the
    # other chains of execution.
    #
    # Better would probably be something like:
    #
    #     p = new_user_promiser(timeout: 3).
    #             then{|user| user.destroy_stuff; user}.
    #             then{|user| user.long_method(timeout: 2)}.
    #             catch{|e|  "Somebody goofed!" }
    #
    #
    # @return [self] with a new rejection path installed on a timer
    #
    def timeout!(seconds)
      defer { 
        sleep seconds; 
        @reject[ TimeoutError.new("exceeded #{seconds} seconds") ] 
      }
      self
    end

    ## Creates a new promise whose success is dependent on `self`.
    #
    # Calling convention: 
    #
    #    promise.then(Proc.new{|value| callback_body }){|err| errback_body }
    def then(callback=nil, &blk)
      LOG.debug{"#{self.inspect}:{#then called"}

      cb = nil
      eb = nil

      if callback.nil?
        cb = blk
      else
        cb = callback
        eb = blk
      end

      # Create a new promise with the blocks
      p = Promise.new( parent:self, callback:cb, errback: eb )
      
      LOG.debug{"#{self.inspect}: adding observer: #{p.inspect}"}
      self.add_observer( p.method(:settle) )

      p
    end

    ## Finally block
    #
    #  Creates a new promise whose block is passed both the 
    #  fulfilled value and the rejected reason (depending on how the parent
    #  resolves), but the value of the block does not affect the promise chaining
    #  process-- the Value/Reason will pass through to any children promises.
    #
    #  This is used to close out resources when an action has completed.
    #
    #  @note any exceptions that are not StandardException
    #  in this block will propagate upwareds (from this functional block point)
    #  and could halt the chain from continuing.
    #
    #  @note the chain will be synchronous with respect to the block provided here.
    #
    def finally(&blk)
      cb = Proc.new{|val| 
        begin
          blk[val, nil]; 
        rescue StandardException    # WARNING: we only trap standard exceptions
        end
        next val 
      }
      eb = Proc.new{|err| 
        begin
          blk[nil, err]
        rescue StandardException    # WARNING: we only trap standard exceptions
        end
        if Exception === err
          raise err   # re-raise
        end
        next err
      }

      p = Promise.new( parent:self, callback:cb, errback: eb )
      self.add_observer( p.method(:settle) )
      p
    end

    ## Returns a promise with a rejection handler for the original `Promise`.  
    #
    # If the parent `Promise` is rejected, this handler is called; if the parent is
    # fulfilled, this promise is fulfilled with the parent's value (pass-through).
    #
    # @return [Promise]
    #
    def catch(&blk)
      self.then(Proc.new{|v| v}, &blk)
    end

    ## Creates a new promise that executes a block on the rejection of the parent `Promise`.
    #
    # This does not *catch* an error-- just allows a quick access to the error for logging
    # or resource release purposes.
    # 
    # This is functionally the equivalent of:
    #
    #     promise.catch{|e| <some_block>; next Promise.reject(e); }
    #
    # @yieldreturn don't care.  Nothing is done with this value.
    #
    def on_error(&blk)
      self.then(Proc.new{|v| v}) do |e| 
        blk.yield e
        Promise.reject( e )
      end
    end

    ## Settle function receives incoming values or exceptions and processes..
    #
    # This is a one-shot function.  `Promise`s can only be settled once-- and observers
    # are informed of this settlement promptly.
    #
    #
    # @return [self]
    #
    def settle(value, error=nil)
      LOG.debug{"#{self.inspect}: #settle called (value=#{value.inspect}; error=#{error.inspect})"}
      synchronize do
        return unless @state == :pending
      end

      rv = nil
      re = nil

      # Handle exception first
      if !error.nil? 
        if @errorback.nil?            # ERROR without errorback
          re = error
          rv = nil
        else
          begin
            rv = @errorback[error]    # May still be a promise that we need to observe
            re = nil
          rescue => e
            re = e
            rv = nil
          end
        end
      else # We received a value
        if !@callback.nil?            # VALUE with callback
          begin
            rv = @callback[value]     # May still be a promise that we need to observe
          rescue => e
            re = e 
            rv = nil
          end
        else                          # VALUE without callback (passthrough)
          rv = value
        end
      end

      LOG.debug{"#{self.inspect} inside #settle, got rv=#{rv.inspect}"}

      # At this point, callbacks/errorbacks are not needed, since they've already been called, 
      # we can clear them out so GC can clean stuff up.
      LOG.debug{"#{self.inspect} clearing out @blk, @callback, @errorback to release block stacks and variables"}
      @blk = @callback = @errorback = nil

      # Register the observer callback when a deferred gets returned as a value
      #
      if !rv.nil?
        if rv.kind_of? Vow::Promise
          LOG.debug{"#{self.inspect} inside #settle: rv is a Promise... adding self as observer"}
          rv.add_observer self.method(:resolve)
          return
        end
      end
      LOG.debug{"#{self.inspect} inside #settle, resolving self"}

      resolve(rv, re)
      self
    end


    ## Makes the Promise either `:fulfilled` or `:rejected`.
    #
    # If Promise *A* depends on Promise *B*, (eg, `A = Promise.new{|f,r| B = some_other_promise_maker }` ), then A will send a reference to its `#resolve`
    # method as an observer of B.
    #
    # When a Promise is finally resolved, it will also inform all its observers of its final state-- using a defered execution of the observer block.
    #
    #
    # @api private
    #
    def resolve( value, error )
      obs = nil
      synchronize do
        @value = value
        @reason = error
        unless @reason.nil?
          @state = :rejected
        else
          @state = :fulfilled
        end

        # Defer is non-blocking
        @observers.each{|o| 
          LOG.debug{"#{self.inspect}: #resolve: informing observer from: #{o.source_location}"}
          defer{ o[value,error] } 
        }
        @observers.clear

        # Signal to anyone waiting on the value
        @event.set
      end
    end
    
    def add_observer( callback=nil, &blk )
      callback ||= blk
      synchronize do
        #puts "Registering observer"
        if @state == :pending
          #puts "  pending; adding as callback"
          @observers << callback
          return
        end
      end
      LOG.debug{"#{self.inspect}: #add_observer: adding to an already resolved Promise; `defer`ing the calback to next cycle"}
      defer { callback[ @value, @reason ] }
    end

    def state
      synchronize do
        return @state
      end
    end

    def fulfill(val=nil)
      settle(val, nil)
    end
    
    # Reject the promise with an error.
    #
    # @param [any] err Error to reject the promise with.  If `err == nil`, then the promise is rejected with `false`.
    #
    def reject(err=false)
      if err.nil?
        err = false
      end
      settle(nil, err)
    end

    def rejected?
      synchronize do
        @state == :rejected
      end
    end

    def fulfilled?
      synchronize do
        @state == :fulfilled
      end
    end
    
    def pending?
      synchronize do
        @state == :pending
      end
    end

    ## Internal synchronization to get state
    #
    # @!visibility private
    #
    def _value(timeout=nil)
      # Even if this is prematurely read as "pending" 
      # when actually set in another thread, Event handles correctly
      state = nil
      synchronize do
        state = @state
      end
      if state == :pending  
        @event.wait(timeout)
      end
      return @value
    end

    ## Check if we need to raise error
    #
    def _value_raise
      if @state == :rejected
        if @reason.kind_of? Exception
          LOG.error{"Promise raising exception from elsewhere: #{@reason.class}: #{@reason.to_s.inspect}"}
          raise @reason
        else
          raise UncaughtPromiseError.new(@reason)
        end
      end
    end

    ## Wait for and get the value of the Promise.
    #
    # @return [nil] on timeout or rejection
    # @return [Object] on fulfilled
    #
    # @note this will raise an exception if the `Promise` was rejected.
    #
    def value(timeout=nil)
      val = _value(timeout)
      _value_raise
      return val
    end

    ## Execute a code block with the value if fulfilled.
    #
    # Since this is executed conditionally, an exception is not raised if the promise is rejected.
    #
    # Does not generate exceptions.
    #
    #
    def tap_fulfilled(timeout=nil, &blk)
      val = _value
      if @state == :fulfilled
        blk.yield val
      end
    end

    ## Return the value of this promise, but with a default in case the promise was rejected.
    # 
    # Does not generate exceptions.
    #
    def value_with_default(obj)
      val = _value
      return (@state == :fulfilled) ? val : obj
    end


    ## Wait for the Promise to be fulfilled or rejected.  
    #
    # @return [Symbol] Promise state (`:fulfilled` or `:rejected`)
    #
    def wait(timeout=nil)
      synchronize do
        state = @state
      end
      LOG.debug{ "Promise#wait: self=#{self.inspect} / state: #{state}" }
      if state == :pending
        @event.wait(timeout)
        return nil
      else
        return nil
      end
    end

    def reason
      synchronize do
        @reason
      end
    end
  end
end

