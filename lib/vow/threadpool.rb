module Vow

  ## Implement a pool of threads
  #
  #
  class ThreadPool
    LOG = MyLogger.new('ThreadPool')
    LOG.level = :error


    class ThreadQueue
      def initialize
        @mutex = Mutex.new
        @cvar  = ConditionVariable.new
        @blocks = []
        @waiting_threads = {}
      end

      ## Returns the number of threads waiting on the queue
      #
      def waiting_count
        @mutex.synchronize do
          return @waiting_threads.count
        end
      end
      alias_method :num_waiting, :waiting_count

      ## Returns an array of the waiting Threads with their :idx fields
      #
      def waiting_threads_idx 
        @mutex.synchronize do
          return @waiting_threads.keys.map{|e| e[:idx]}
        end
      end
      
      ## Returns the number of items in the queue
      #
      def length
        @mutex.synchronize do
          return @blocks.length
        end
      end
      alias_method :count, :length
      alias_method :size, :length

      ## Remove an element; block and wait for one to become available.
      #
      # @return [Object]
      #
      def shift
        @mutex.synchronize do
          if @blocks.length > 0
            LOG.debug{"ThreadQueue#shift, block ready; no wait"}
            @waiting_threads.delete Thread.current
            LOG.debug{ "waiting_threads: #{@waiting_threads.map{|k,v| k[:idx]}.inspect}" }

            return @blocks.shift 
          end
          
          loop do
            @waiting_threads[ Thread.current ] = true

            LOG.debug{ "**$$** @cvar.wait" }
            @cvar.wait @mutex
            LOG.debug{ "woke" }

            break if @blocks.length > 0
            LOG.debug{ "Spurious wakeup" }
          end
            

          @waiting_threads.delete Thread.current
          LOG.debug{ "**__** waiting_threads: #{@waiting_threads.map{|k,v| k[:idx]}.inspect}; blks=#{@blocks.length}" }
          return @blocks.shift
        end
      end

      ## Add an element
      #
      # Returns the number of blocks and currently waiting threads
      #
      # @return [Fixnum, Fixnum] [waiting_blocks, waiting_threads]
      #
      def push(blk)
        @mutex.synchronize do
          @blocks.push blk

          LOG.debug{("**!!** abt to signal. PUSH : blks=#{@blocks.length}") }
          @cvar.signal

          return [ @blocks.length, @waiting_threads.count ]
        end
      end
      alias_method :<<, :push

      ## Add a thread to the wait list, even if it cannot add itself yet by calling `#shift`.
      #
      def pre_wait(thread)
        @mutex.synchronize{ @waiting_threads[ thread ] = true }
        LOG.debug{("ThreadQueue#pre_wait: threads=#{waiting_threads_idx.inspect}") }
      end

    end # class ThreadQueue




    ## List of all threads
    @@thread_list = []
    ## Current limit for active threads created for pool
    @@thread_limit = 20 
    ## Synchronization queue for tasks
    @@queue = ThreadQueue.new
    ## Synchronization lock
    @@lock = Mutex.new
    ## Current index count of threads
    @@thread_idx = 0

    ## For threads that are created but not fully waiting
    @@in_process_mutex = Mutex.new
    ## In-process list
    @@in_process = {}

    ## Reset the threadpool to initialization state.
    #
    # @note This will kill all threads that are waiting and reset the pool to its initial state.
    #
    # Only call this from the outermost thread
    #
    # @return [nil]
    #
    def self.reset!
      @@lock.synchronize do
        LOG.warn{ "=================================================" }
        LOG.warn{ "#reset! #=> RESTTING STATE" }
        LOG.warn{ "=================================================" }
        @@thread_list.each{|t| t.kill }
        @@thread_list = []
        @@queue      = ThreadQueue.new
        @@thread_idx = 0
      end
      @@lock = Mutex.new
      nil
    end

    ## Set the thread limit
    #
    # @param [Fixnum] value
    #
    # @return [Fixnum] value
    #
    def self.thread_limit= value
      @@thread_limit = value
      # TODO-FEATURE: Add a termination signal to threads when the limit is reduced
    end

    def self.check_spawn2
      LOG.info{ "check_spawn2; number of waiting blocks: #{@@queue.length}; nThreads=#{@@thread_list.length}, limit=#{@@thread_limit}" }

      if @@thread_list.length == 0 or (
          @@queue.length > 1 and @@thread_list.length < @@thread_limit
      )
        LOG.info{ "spawning new thread..." }
        t = self.spawn_thread
        @@thread_list << t
      end
      nil
    end


    ## Not sure
    #
    # @todo Add information
    #
    #
    def self.check_spawn

      waiting_threads = @@queue.num_waiting + @@in_process_mutex.synchronize{ @@in_process.keys.length } 
      LOG.info{ "#check_spawn; idle_threads=#{@@queue.num_waiting} total_threads=#{@@thread_list.length}" }

      if waiting_threads == 0 and @@thread_list.length < @@thread_limit
        LOG.info{ "num_waiting is 0; spawning new thread." }

        t = self.spawn_thread
        @@thread_list << t

      end
    end

    def self.report
      alive_threads = @@thread_list.select{|e| e.alive?}.count
      "Total threads: #{@@thread_list.length}\nAlive threads: #{alive_threads}\nIdlee threads (waiting-in-@@queue): #{@@queue.num_waiting}\nAllThreadInfo:#{@@thread_list.inspect}"

    end


    ## Queue a block to be done next
    #
    # @note calls a `Thread.pass` before adding the block to the `@@queue`.  This allows any just-created
    # thread to be ready to receive the block.
    #
    def self.next_do( &blk )
      # TODO: see if there is a faster way to do this without locking each time.
      @@lock.synchronize do
        @@queue << blk
        LOG.debug{ "next_do called; @@lock synchronized;      BLOCK added to @@queue; doing check_spawn2" }
        self.check_spawn2
      end
    end

    ## Replacement Queue a block to be done next
    #
    def self.next_do( &blk )
      @@lock.synchronize do
        LOG.debug{ "ThreadPool.next_do(...), pushing to @@queue" }
        waiting_blocks, waiting_threads = @@queue.push blk
        LOG.debug{ "ThreadPool.next_do(...), waiting_blocks=#{waiting_blocks}; waiting_threads=#{waiting_threads}." }
        LOG.debug{ "... waiting_threasds=#{ @@queue.waiting_threads_idx }" }
        threads_needed_extra = waiting_blocks - waiting_threads + 1    # one spare

        threads_to_add = [ @@thread_limit - @@thread_list.count, threads_needed_extra ].min

        LOG.info{ "Need to add #{threads_to_add} threads to satisfy demand; spawning..." }
        threads_to_add.times{ @@thread_list << self.spawn_thread }
      end
    end

    def self.thread_count
      @@lock.synchronize do
        @@thread_list.length
      end
    end

    def self.waiting_count
      @@lock.synchronize do
        @@queue.num_waiting 
      end
    end

    ## Spawn a new thread.  
    #
    # New thread executes a loop to wait for new blocks and executes the blocks.
    # 
    def self.spawn_thread
      
      idx = @@thread_idx += 1
      LOG.warn{ "Spawning Thread: idx; #{idx}" }

      t = Thread.new do

        # Let outside know we are started well.
        LOG.debug{ "inside new thread: #{idx}" }
        #
        # Main loop
        loop do

          LOG.debug{ "#spawn_thread - Main Loop - waiting to take blk" }
          blk = @@queue.shift


          LOG.debug{ "#spawn_thread - Main Loop - Received block with source: #{blk.source_location}" }
          LOG.debug("#spawn_thread - Main Loop - ThreadQueue.waiting_threads=#{@@queue.waiting_threads_idx.inspect}")
          begin
            blk.call
          rescue Exception  => e
            LOG.error("#{Thread.current}: t#{Thread.current[:idx]}: caught exception: #{e.class}: #{e}")
          rescue Exception => e
            LOG.error("#{Thread.current}: t#{Thread.current[:idx]}: caught SERIOUS exception: #{e.class}: #{e}::: #{e.backtrace.join("\n\t")}")
          end
        end
      end

      t[:idx] = idx
      @@queue.pre_wait t
      t
    end
  end
end
