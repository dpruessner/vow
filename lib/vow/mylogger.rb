class MyLogger
  @@mutex = Mutex.new
  @@ary = []
  @@start_time = Time.now
  @@cvar = ConditionVariable.new
  @@thread = Thread.new do
    Thread.current[:idx] = -1
    loop do
      @@mutex.synchronize do
        @@ary.length.times{ line = @@ary.shift; puts line; }
        @@cvar.wait(@@mutex, nil)
      end # mutex
    end   # loop
  end

  def initialize(name=nil)
    @name = name
    @level = 0
    Thread.pass
  end
  def level=(val)
    case val
    when :debug
      @level = 4
    when :info
      @level = 3
    when :warn
      @level = 2
    when :error
      @level = 1
    when :none
      @level = 0
    else
      @level = val
    end
  end
  def _format(str)

    thread_str = Thread.current[:idx] ? ("%03i" % [Thread.current[:idx]]) : "xxx"

    time_str = "+%9.5f" % Time.now.-(@@start_time)

    str = "#{time_str}  #{"%12s" % @name} T[#{thread_str}]: #{str}"
    str
  end

  def _print(str)
    str = _format(str)
    @@mutex.synchronize do
      #puts str
      @@ary << str
      @@cvar.signal
    end
  end
  def flush!(str="")
    @@mutex.synchronize do
      @@ary.length.times{ puts @@ary.shift }
      puts _format(str) if str.length > 0
      $stdout.flush
    end
  end

  def error(str=nil, &blk)
    return if @level < 1
    _print("ERROR : " + str) if str
    _print("ERROR : " + blk.yield) if blk
  end
  def warn(str=nil, &blk)
    return if @level < 2
    _print("WARN  : " + str) if str
    _print("WARN  : " + blk.yield) if blk
  end
  def info(str=nil, &blk)
    return if @level < 3
    _print("INFO  : " + str) if str
    _print("INFO  : " + blk.yield) if blk
  end
  def debug(str=nil, &blk)
    return if @level < 4
    _print("DEBUG : " + str) if str
    _print("DEBUG : " + blk.yield) if blk
  end
end
