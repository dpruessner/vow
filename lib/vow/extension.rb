module Vow

  module ObjectExtension
    def thenable?
      self.respond_to? :then
    end
    def settleable?
      self.respond_to? :settle
    end
    def observable?
      self.respond_to? :observe
    end
  end

end

class Object
  include Vow::ObjectExtension
end
