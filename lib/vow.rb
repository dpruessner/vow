#
#
#

module Vow
  VERSION="1.0.2"

  # Error that timeouts will throw.
  class TimeoutError < RuntimeError; end

end


# For instrumentation
require 'vow/mylogger.rb'

require 'vow/threadpool'
require 'vow/promise'
require 'vow/extension'


