Gem::Specification.new do |s|
  s.name        = 'vow'
  s.version     = '1.0.2'
  s.date        = '2017-06-06'
  s.summary     = "Promises for Ruby"
  s.description = "A concurrency library implementing A+ like promises with Threads and EventMachine; release a91717f"
  s.authors     = ["Daniel Pruessner"]
  s.email       = 'daniel@sprinkl.io'
  s.files       = Dir.glob 'lib/**/*.rb'
  s.homepage    = 'http://sprinkl.io/'
  s.license       = 'Nonstandard'
end

